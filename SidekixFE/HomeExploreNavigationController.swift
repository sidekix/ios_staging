//
//  HomeExploreNavigationController.swift
//  SidekixFE
//
//  Created by Sea Pong on 7/30/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

import UIKit

class HomeExploreNavigationController: SidekixMenuNavigationController, SidekixMenuDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        menu = SidekixMenu(sourceView: self.view, menuViewController: MenuViewController(), menuPosition:.Right)
        //sideMenu?.delegate = self //optional
        menu?.menuWidth = 180.0 // optional, default is 160
        //sideMenu?.bouncingEnabled = false

        // make navigation bar showing over side menu
        view.bringSubviewToFront(navigationBar)

        self.title = "Sidekix Navigation Controller"


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - ENSideMenu Delegate
    func MenuWillOpen() {
        printMessage("sideMenuWillOpen")
    }

    func MenuWillClose() {
        printMessage("sideMenuWillClose")
    }

    func MenuShouldOpenMenu() -> Bool {
        printMessage("sideMenuShouldOpenSideMenu")
        return true
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
