//
//  HomeExploreViewController.swift
//  SidekixFE
//
//  Created by Sea Pong on 1/27/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

import Foundation

import UIKit


class HomeExploreViewController: UIViewController, SidekixMenuDelegate {
    
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var sidekixMenuLogo: UIButton!

    
    @IBAction func menuButtonAction(sender: UIBarButtonItem) {
        printMessage("Menu Button Activated")

        toggleSideMenuView()


    }



    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        var navigationBarAppearace = UINavigationBar.appearance()
        
        // Bar Color
        navigationBarAppearace.barTintColor = UIColor(red: (0x00/0xFF), green: (0xA8/0xFF), blue: (0xFF/0xFF), alpha: 1)

        // Button Color
        navigationBarAppearace.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)


        // Bar Title Attributes
        let titleTextAttribures: NSDictionary = [NSForegroundColorAttributeName: UIColor(red: 1, green: 1, blue: 1, alpha: 1), NSFontAttributeName: "FontAwesome"]
        navigationBarAppearace.titleTextAttributes = titleTextAttribures as [NSObject : AnyObject];

        sidekixMenuLogo.imageView?.contentMode = UIViewContentMode.ScaleAspectFit;
        //            addPhotoButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit

        let customFont = UIFont(name: "FontAwesome", size: 17.0)


        self.title = "Home Explore View Controller"

        self.sideMenuController()?.menu?.delegate = self



    }

      
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }




    // MARK: - ENSideMenu Delegate
    func MenuWillOpen() {
        printMessage("sideMenuWillOpen")
    }

    func MenuWillClose() {
        printMessage("sideMenuWillClose")
    }

    func MenuShouldOpenMenu() -> Bool {
        printMessage("sideMenuShouldOpenSideMenu")
        return true
    }

    
}

