//
//  LoginViewController.swift
//  SidekixFE
//
//  Created by Sea Pong on 1/18/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

import UIKit
import Foundation

class LoginViewController: UIViewController, FBLoginViewDelegate {
    
    @IBOutlet var fbLoginView : FBLoginView!
    
    @IBOutlet weak var loginDebug: UILabel!
    
    let transitionManager = TransitionManager();
    
    override func viewDidLoad() {

        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        // Set this login view controller to be the fbLogin delegate
        self.fbLoginView.delegate = self

        // Ask the user for these permissions
        self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends"]

        // Set the login text
        self.loginDebug.text = "Sign Up or Login"

        self.title = "Login View Controller"
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // this gets a reference to the screen that we're about to transition to
        let toViewController = segue.destinationViewController as! UIViewController
        
        // instead of using the default transition animation, we'll ask
        // the segue to use our custom TransitionManager object to manage the transition animation
        toViewController.transitioningDelegate = self.transitionManager


        
    }
    
    // override this UIViewController method to manage what style status bar is shown
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return self.presentingViewController == nil ? UIStatusBarStyle.Default : UIStatusBarStyle.LightContent
    }
    
    // MARK: Facebook Delegate Methods
    
    // Tells the delegate that the view is now in logged in mode
    func loginViewShowingLoggedInUser(loginView : FBLoginView!) {

        // Automatically perform segue to Home Explore If User is logged in
        printMessage("User Logged in via Facebook - Performing Segue to HOME EXPLORE...")
        //self.performSegueWithIdentifier("SegueLoggedIn", sender: self)



        //Present new view controller
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : AnyObject!

        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("profile_nav_ctrl")

        self.showViewController(destViewController as! UIViewController, sender: self)

    }
    

    // Tells the delegate that the view is has now fetched user info
    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser){

        printMessage("User Logged in via Facebook - Fetching Profile Data...")


        loginDebug.text = "Welcome Facebook User - \(user.name) " ;

        // Correct url and username/password
        // Get Sidekix Login Information and Populate Login Global Object
//        postDataToServer(params: ["username":"jameson", "password":"password"], url: "\(SidekixGlobalParameters.sharedInstance.httpRoot)test.php" ) { (succeeded: Bool, msg: String) -> () in
//            var alert = UIAlertView(title: "Success!", message: msg, delegate: nil, cancelButtonTitle: "Okay.")
//            if(succeeded) {
//                alert.title = "Success!"
//                alert.message = msg
//            }
//            else {
//                alert.title = "Failed : ("
//                alert.message = msg
//            }

            // Move to the UI thread
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                // Show the alert
//                alert.show()
//            })
        //}


        
    }
    
    // Tells the delegate that the view is now in logged out mode
    func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
        printMessage("User Logged Out")
        self.loginDebug.text = "Sign Up or Login"

        
    }
    
    //  Tells the delegate that there is a communication or authorization error.
    func loginView(loginView : FBLoginView!, handleError:NSError) {
        printMessage("Error: \(handleError.localizedDescription)")
    }
    
    
    // MARK: Move this to external library s
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

