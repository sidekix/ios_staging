//
//  MenuViewController.swift
//  SidekixFE
//
//  Created by Sea Pong on 7/30/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

import UIKit

class MenuViewController: SidekixUITableViewController{


    

    var selectedMenuItem : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Customize apperance of table view
        tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0) //
        tableView.separatorStyle = .None
        tableView.backgroundColor = UIColor.clearColor()
        tableView.scrollsToTop = false

        //set table view background to black

        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = true

        //tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)


        listSubviewsOfView(self.tableView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 6
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {


        var cell = tableView.dequeueReusableCellWithIdentifier("CELL") as? UITableViewCell

        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.whiteColor()
            let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height))
            selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
            cell!.selectedBackgroundView = selectedBackgroundView
        }

        var textLabel : String = "";
        switch indexPath.row {
            case 0: textLabel = "Explore"
            case 1: textLabel = "My Profile"
            case 2: textLabel = "Add Skill Photo"
            case 3: textLabel = "Chat"
            case 4: textLabel = ""

            case 5: textLabel = "Beacon Mode"

        default: textLabel = "";
        }
        cell!.textLabel?.text = textLabel;
        //cell!.textLabel!.font = UIFont(name: "FontAwesome", size: 17.0)
        cell!.textLabel?.textAlignment = .Center;

        if(indexPath.row > 4) {
            cell!.textLabel!.font = UIFont.boldSystemFontOfSize(17.0)
        }

        return cell!
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        println("Selected row: \(indexPath.row)")

//        if (indexPath.row == selectedMenuItem) {
//            hideSideMenuView();
//            return
//        }

        selectedMenuItem = indexPath.row

        //Present new view controller
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : AnyObject!

        switch (indexPath.row) {
        case 0:
            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("home_explore_nav_ctrl")
        case 1:
            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("profile_nav_ctrl")
        case 2:
            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController3")
        default:
            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController3")

        }


        self.showViewController(destViewController as! UIViewController, sender: self)
        //self.presentViewController(destViewController as! UIViewController, animated: false, completion: nil)

        //sideMenuController()?.setContentViewController(destViewController)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
