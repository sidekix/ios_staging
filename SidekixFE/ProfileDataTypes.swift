//
//  ProfileDataTypes.swift
//  SidekixFE
//
//  Created by Sea Pong on 9/12/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

class ProfileData {

    var getID : Int = 0
    var getIDValid : Bool = false

    var userID : Int = 0

    var firstName : String = ""
    var middleName : String = ""
    var lastName : String = ""

    var profilePictureID : String = ""

    var facebookLinked : Bool = false
    var facebookID : Int = 0

    var joinDate : Int = 0
    var lastLoginDate : Int = 0

    var admin : Bool = false

    var biography : String = ""
    var htmlLink : String = ""

    var homeLocation : Location = Location()
    var currLocation : Location = Location()



    var skills : [ProfileSkillData] = []


    // Provides for a forward pointer to access the cell element from this data class instance
    var viewInstance : ProfileViewController? = nil


    var ready : Bool = false;


    init (viewcontroller: ProfileViewController, id: Int) {
        self.viewInstance = viewcontroller;

        self.getID = id
        self.getIDValid = true;

        queryDBandPopulate()
    }


    func queryDBandPopulate(){
        if getIDValid {
            //Get Sidekix Login Information and Populate Login Global Object
            postDataToServer(
                params: ["id":String(getID)],
                url: "\(SidekixGlobalParameters.sharedInstance.httpRoot)profile.php?id=\(String(getID))",
                postCompleted:{
                    (succeeded: Bool, msg: String, data: NSDictionary?) -> () in

                    printMessage("Query DB Done" )
                    if(succeeded) {

                        //printMessage("\(data)")

                        var name = data!.valueForKeyPath("profile_data.first_name")! as! String

                        printMessage("Profile Fetched for \(name)" )

                        self.userID = self.getID

                        self.firstName = data!.valueForKeyPath("profile_data.first_name")! as! String
                        self.middleName = ""
                        self.lastName = data!.valueForKeyPath("profile_data.last_name")! as! String

                        self.profilePictureID = ""

                        self.facebookLinked = false
                        self.facebookID = 0

                        self.joinDate = 0
                        self.lastLoginDate = 0

                        self.admin = false

                        self.biography = ""
                        self.htmlLink = ""

                        self.homeLocation = Location()
                        self.currLocation = Location()



                        var skills_raw : NSArray = data!.valueForKey("skills") as! NSArray

                        for skill_raw in skills_raw {
                            let skill_data = skill_raw as! NSDictionary;

                            //                                printMessage("\(skill_data)")
                            //                                for (key, value) in skill_data {
                            //                                    println("Property: \"\(key as! String)\"")
                            //                                }

                            // Initialize new object and push into queue
                            self.skills.append(ProfileSkillData())

                            // Populate the object
                            self.skills.last!.name = skill_data["skill_name"] as! String;
                            self.skills.last!.photos.append(UIImage(named: "Yosemite 1")!)
                            self.skills.last!.photos.append(UIImage(named: "Yosemite 2")!)
                            self.skills.last!.photos.append(UIImage(named: "Yosemite 3")!)

                            printMessage("adding skill: \(self.skills.last!.name)")

                        }
                        printMessage("total skills: \(self.skills.count)")


                        self.ready = true

                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.viewInstance?.reloadData()
                        })

                    }



                    //Move to the UI thread
                    //                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //                            // Show the alert
                    //                            alert.show()
                    //                        })
            })

        }


    }


}




class ProfileSkillData {
    
    // Indicates whether or not the panel is exapnded in the current profile view
    var expansion : Bool =  false
    
    // Indicates the skill name that is printed on the tab main bar
    var name : String = "Blank Skill"
    
    // Indicates whether or not the current logged in user has bookmarked this person's skill
    var bookmarked : Bool = false
    
    // Indicates the database ID of this skill
    var d_skillID : Int = 0
    
    // Indicates the database ID of this profile Skill
    var d_profileSkillID : Int = 0
    
    // Establish an array to store photos in the Profile Skill Table Cell View
    var photos : [UIImage] = []
    
    // Provides for a forward pointer to access the cell element from this data class instance
    var cellInstance : ProfileSkillTableViewCell? = nil
    
    
}

