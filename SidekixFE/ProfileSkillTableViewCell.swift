//
//  ProfileSkillTableViewCell.swift
//  SidekixFE
//
//  Created by Sea Pong on 8/18/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

import Foundation

class ProfileSkillTableViewCell : UITableViewCell{

    @IBOutlet var expansionButton: UIButton!
    @IBOutlet var bookmarkButton: UIButton!
    @IBOutlet var skillBar: UIView!
    @IBOutlet var skillName: UILabel!

    @IBOutlet var photoScrollView: UIScrollView!


    var controller : ProfileSkillTableViewController! = nil

    var cellDataObject : ProfileSkillData! = nil

    var indexPath : NSIndexPath! = nil;



    @IBAction func didTouchExpandButton(sender: AnyObject) {
        printMessage("Skill Expand Button Touch Up Action", verbosity: .high)
        controller.toggleCellExpansion(self);

        layoutIfNeeded();

    }

    @IBAction func didTouchBookmarkButton(sender: AnyObject) {
        printMessage("Skill Bookmark Button Touch Up Action", verbosity: .high)
        controller.toggleCellBookmark(self);

        layoutIfNeeded();

    }

    // This function is called quite frequently and should lightweight. Only relayout should be done here
    override func layoutSubviews() {
        super.layoutSubviews()


        let scrollViewWidth : CGFloat = self.photoScrollView.frame.width;
        let scrollViewHeight : CGFloat = self.photoScrollView.frame.height;

        printMessage("PhotoScrollView created for \(cellDataObject.name) - \(scrollViewWidth)x\(scrollViewHeight)", verbosity: .high)

        var photoCount = 0
        for photo in cellDataObject.photos {
            //func CGRectMake(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> CGRect
            var photoView = UIImageView(frame: CGRectMake((scrollViewWidth * CGFloat(photoCount)), (0), (scrollViewWidth), (scrollViewHeight)))
            photoView.image = photo;
            photoView.contentMode = .ScaleAspectFill
            printMessage("Add Photo to Skill \(photo.description)", verbosity: .debug)

            photoScrollView.addSubview(photoView)
            photoCount++
        }

        photoScrollView.contentSize = CGSizeMake(scrollViewWidth * CGFloat(photoCount), scrollViewHeight)




        photoScrollView.delegate = controller
        

    }


    func tappedSkillBar(){
        printMessage("Skill Expand Button Touch Up Action", verbosity: .high)
        controller.toggleCellExpansion(self);

        layoutIfNeeded();
    }




}