//
//  ProfileSkillTableViewController.swift
//  SidekixFE
//
//  Created by Sea Pong on 8/18/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

import Foundation

import UIKit


class ProfileSkillTableViewController: UITableViewController {


    var skillCellData : [ProfileSkillData] = []


    var parent_ProfileViewController : ProfileViewController! = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        printMessage("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__) @ line\(__LINE__) ");

        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.blackColor()

        tableView.sectionIndexColor = UIColor.blackColor()
        tableView.sectionIndexBackgroundColor = UIColor.blackColor()


        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40.0

    }


    func populateProfileSkillData(){

        printMessage("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__) @ line\(__LINE__) ");

        //parent_ProfileViewController.profileData = ProfileData(id:2)

//        parent_ProfileViewController.profileData?.getID = 2
//        parent_ProfileViewController.profileData?.getIDValid = true
//        parent_ProfileViewController.profileData?.queryDBandPopulate()

        skillCellData = parent_ProfileViewController.profileData!.skills

        printMessage("displaying \(skillCellData.count) skills...")


        for skill in skillCellData {

            printMessage("displaying skill: \(skill.name)")

        }




//        // Clear skillCellData
//        skillCellData = []
//
//        // Authenticate and populate [PROFILE] data from server
//        //skillCellData = [ProfileSkillData](count: 5, repeatedValue: nil)
//
//
//        // dummy data
//        skillCellData.append(ProfileSkillData())
//        skillCellData.last!.name = "Apples"
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 1")!)
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 2")!)
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 3")!)
//
//        skillCellData.append(ProfileSkillData())
//        skillCellData.last!.name = "Bananas"
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 4")!)
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 5")!)
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 2")!)
//
//        skillCellData.append(ProfileSkillData())
//        skillCellData.last!.name = "Cherries"
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 2")!)
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 5")!)
//        skillCellData.last!.photos.append(UIImage(named: "Yosemite 4")!)
//
//        skillCellData.append(ProfileSkillData())
//        skillCellData.last!.name = "Doggies"
//        skillCellData.append(ProfileSkillData())
//        skillCellData.last!.name = "Elephant"
//        // end dummy data




        printMessage("Number of " + ((parent_ProfileViewController.isMyself) ? ("own") : ("other"))  + " skills loaded: \(skillCellData.count)");

            //                            // Show the alert
            //                            alert.show()
            //                        })
            self.tableView.reloadData()


        tableView.beginUpdates()
        tableView.endUpdates()

    }

    override func viewWillAppear(animated: Bool) {
        //tableView.reloadData()
        printMessage("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__) @ line\(__LINE__) ");

    }






    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> ProfileSkillTableViewCell {
        printMessage("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__) @ line\(__LINE__) ");

        printMessage("Skill Cell created: \(skillCellData[indexPath.row].name)", verbosity: .high)

        let cell = tableView.dequeueReusableCellWithIdentifier("SkillCell", forIndexPath: indexPath) as! ProfileSkillTableViewCell


        cell.controller = self as ProfileSkillTableViewController
        cell.indexPath = indexPath as NSIndexPath

        cell.textLabel?.hidden = true

        cell.skillName.text = skillCellData[indexPath.row].name

        cell.skillName.textColor = UIColor.whiteColor()
        cell.backgroundColor = UIColor.blackColor()


        //cell.cellDesc = "\(cell.skillName.text) - S\(indexPath.section)R\(indexPath.row)"

        let tap = UITapGestureRecognizer()
        tap.addTarget(cell, action: "tappedSkillBar")
        cell.skillBar.addGestureRecognizer(tap)
        cell.skillBar.userInteractionEnabled = true


        
        cell.photoScrollView.pagingEnabled = true



        // Create back-pointer to link from cellData to cell
        skillCellData[indexPath.row].cellInstance = cell

        // Create forward-pointer to link from cell to cellData
        cell.cellDataObject = skillCellData[indexPath.row]



        return cell
    }


    //    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    //        var selectedRowIndex = indexPath
    //        currentRow = selectedRowIndex.row
    //    }

    func updateCellHeights(){

    }

    func toggleCellExpansion(cell: ProfileSkillTableViewCell){
        if skillCellData[cell.indexPath.row].expansion {
            skillCellData[cell.indexPath.row].expansion = false

            //cell.expansionButton.setTitle("+", forState: UIControlState.Normal)
            //cell.expansionButton.transform = CGAffineTransformMakeRotation( CGFloat(0) )

            UIView.animateWithDuration(1.0,
                delay: 0,
                usingSpringWithDamping: 0.4,
                initialSpringVelocity: 8.0,
                options: UIViewAnimationOptions.AllowUserInteraction,
                animations: {
                    cell.expansionButton.transform = CGAffineTransformMakeRotation( CGFloat(0)      )
                }, completion: nil)

        }

        else{

            skillCellData[cell.indexPath.row].expansion = true
            //cell.expansionButton.setTitle("-", forState: UIControlState.Normal)
            //cell.expansionButton.transform = CGAffineTransformMakeRotation( CGFloat(M_PI/2.0) )

            UIView.animateWithDuration(1.0,
                delay: 0,
                usingSpringWithDamping: 0.4,
                initialSpringVelocity: 8.0,
                options: UIViewAnimationOptions.AllowUserInteraction,
                animations: {
                    cell.expansionButton.transform = CGAffineTransformMakeRotation( CGFloat(M_PI/2.0) )
                }, completion: nil)
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }


    func toggleCellBookmark(cell: ProfileSkillTableViewCell){
        if skillCellData[cell.indexPath.row].bookmarked {
            skillCellData[cell.indexPath.row].bookmarked = false

            //cell.expansionButton.setTitle("+", forState: UIControlState.Normal)
            //cell.expansionButton.transform = CGAffineTransformMakeRotation( CGFloat(0) )

            UIView.animateWithDuration(1.0,
                delay: 0,
                usingSpringWithDamping: 0.8,
                initialSpringVelocity: 8.0,
                options: UIViewAnimationOptions.AllowUserInteraction,
                animations: {
                    cell.bookmarkButton.setBackgroundImage(UIImage(named: "skillBookmarkInactive"), forState: UIControlState.Normal)
                }, completion: nil)


        }

        else{

            skillCellData[cell.indexPath.row].bookmarked = true
            //cell.expansionButton.setTitle("-", forState: UIControlState.Normal)
            //cell.expansionButton.transform = CGAffineTransformMakeRotation( CGFloat(M_PI/2.0) )
            UIView.animateWithDuration(1.0,
                delay: 0,
                usingSpringWithDamping: 0.8,
                initialSpringVelocity: 8.0,
                options: UIViewAnimationOptions.AllowUserInteraction,
                animations: {
                    cell.bookmarkButton.setBackgroundImage(UIImage(named: "skillBookmarkActive"), forState: UIControlState.Normal)

                }, completion: nil)

        }

        printMessage("Skill Bookmark Button Touch Up Action", verbosity: .high)
        printMessage("Skill Bookmark Button Action on \"\(skillCellData[cell.indexPath.row].name)\" -  now: \(skillCellData[cell.indexPath.row].bookmarked)", verbosity: .high)

        tableView.beginUpdates()
        tableView.endUpdates()
    }



    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height : CGFloat = 40.0

        if skillCellData[indexPath.row].expansion{
            height = 260.0
        }

        else{
            height = 44.0
        }
        printMessage("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__) @ line\(__LINE__) " + "Category \(indexPath.section) Row \(indexPath.row) Height \(height)", verbosity: .debug);


        return height
    }





    // Returns the number of skills in each section. *** SIDEKIX 1.0 only supports 1
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { return skillCellData.count }
        return 0
    }
    
    // Returns the number of sections. ***SIDEKIX 1.0 only supports 1
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // Returns the title for the header in each section. *** SIDEKIX 1.0 only supports 1
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Skills"
    }
    
    // Returns the height of title for the header in each section. *** SIDEKIX 1.0 only supports 1, so ZERO out
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
}
