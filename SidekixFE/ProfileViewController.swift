//
//  ProfileViewController.swift
//  SidekixFE
//
//  Created by Sea Pong on 7/23/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//
import Foundation

import UIKit


class ProfileViewController: UIViewController, SidekixMenuDelegate {


    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var sidekixMenuLogo: UIButton!

    @IBOutlet var profileSkillTableViewContainer: UIView!

    // Indicates whether or not the current profile is the logged in user's profile
    var isMyself : Bool = false



    var profileData : ProfileData? = nil



    @IBAction func menuButtonAction(sender: UIBarButtonItem) {
        printMessage("Menu Button Activated")

        toggleSideMenuView()

        printMessage("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__)");


    }



    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        var navigationBarAppearace = UINavigationBar.appearance()

        // Bar Color
        navigationBarAppearace.barTintColor = UIColor(red: (0xFF/0xFF), green: (0x57/0xFF), blue: (0x00/0xFF), alpha: 1)

        // Button Color
        navigationBarAppearace.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)


        // Bar Title Attributes
        let titleTextAttribures: NSDictionary = [NSForegroundColorAttributeName: UIColor(red: 1, green: 1, blue: 1, alpha: 1), NSFontAttributeName: "FontAwesome"]
        navigationBarAppearace.titleTextAttributes = titleTextAttribures as [NSObject : AnyObject];

        sidekixMenuLogo.imageView?.contentMode = UIViewContentMode.ScaleAspectFit;
        //            addPhotoButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit

        let customFont = UIFont(name: "FontAwesome", size: 17.0)


        self.title = "Profile View Controller"

        self.sideMenuController()?.menu?.delegate = self

//        tableView.backgroundView = nil;
//        tableView.backgroundColor = UIColor.darkGrayColor()

        printMessage("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__)");

        // Create forward-pointer to the embedded table view controller
        let profileSkillTableViewController = self.childViewControllers.first as! ProfileSkillTableViewController

        // Create back-pointer from the enbedded table view controller to this controller
        profileSkillTableViewController.parent_ProfileViewController  = self




        profileData = ProfileData(viewcontroller: self, id: 2)




    }


    func reloadData(){

        // Create forward-pointer to the embedded table view controller
        let profileSkillTableViewController = self.childViewControllers.first as! ProfileSkillTableViewController

        // Create back-pointer from the enbedded table view controller to this controller
        profileSkillTableViewController.parent_ProfileViewController  = self



        // iOS will try to fully load the internal table view first. To overcome that
        // we dont do anything in the skillTableView.viewWillLoad, but instead keep that lightweight.
        // Let the main profileViewController load FIRST, then go back and invoke a custom function
        // to completely load the table view.
        // TODO: Probably a better way to do this without using a container view
        profileSkillTableViewController.populateProfileSkillData()
        //profileSkillTableViewController.tableView.beginUpdates()
        //profileSkillTableViewController.tableView.endUpdates()



    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }





    // MARK: - ENSideMenu Delegate
    func MenuWillOpen() {
        printMessage("sideMenuWillOpen")
    }

    func MenuWillClose() {
        printMessage("sideMenuWillClose")
    }
    
    func MenuShouldOpenMenu() -> Bool {
        printMessage("sideMenuShouldOpenSideMenu")
        return true
    }

    
    
}


