//
//  SidekixGlobalParameters.swift
//  SidekixFE
//
//  Created by Sea Pong on 1/29/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

import Foundation

class SidekixGlobalParameters : NSObject{

    // Singleton Class
    static var sharedInstance : SidekixGlobalParameters = SidekixGlobalParameters();

    var uuid = NSUUID()

    var httpRoot : String = "http://www.sidekixapp.com/staging/SideKix/src/www/"

    // Higher the number (1-5) the more verbose
    var message_verbosity_threshold : SidekixDebugVerbosity = .high;
    // Sidekix Debug Prefix
    var message_prefix : String = "SidekixFE" as String;

    // METHODS
    override init() {
        println("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__) Singleton Initialized \(uuid)");
    }

    func printUUID(){
        println("\(NSStringFromClass(self.dynamicType)).\(__FUNCTION__) Singleton Accessed \(uuid)");
    }

    func getHTTPRoot() -> String{
        printUUID();
        return httpRoot;
    }

}

public enum SidekixDebugVerbosity : Int8 {
    case fatal = -1
    case error = 0
    case debug = 5
    case high = 4
    case medium = 3
    case low = 2
    case none = 1

    func simpleDescription() -> String {
        switch self {
        case .fatal:
            return "FATAL"
        case .error:
            return "ERROR"
        case .debug:
            return "DEBUG"
        case .high:
            return "HIGH"
        case .medium:
            return "MEDIUM"
        case .low:
            return "LOW"
        case .none:
            return "NONE"        }
    }
}

func printViewControllerStack(){

    var currentVC = UIApplication.sharedApplication().keyWindow?.rootViewController
    printMessage("Printing View Controllers - Starting VC = \(currentVC!.title)", verbosity: .high)


    while let childVC : UIViewController? = currentVC!.presentedViewController {
        if ( currentVC == nil ){ break }
        printMessage("------------------------------------------", verbosity: .high)
        printMessage("Traversing View Controllers - currentVC = \(currentVC!.title)", verbosity: .high)
        printMessage("Traversing View Controllers - childVC   = \(childVC?.title)", verbosity: .high)
        currentVC = childVC
    }


}

func printMessage(message: NSString, verbosity : SidekixDebugVerbosity = .medium){
    if((SidekixGlobalParameters.sharedInstance.message_verbosity_threshold.rawValue) >= verbosity.rawValue) {
        println(SidekixGlobalParameters.sharedInstance.message_prefix + " [" + verbosity.simpleDescription() + "] - " + (message as String));
    }

}

func postDataToServer(#params : Dictionary<String, String>, #url : String, #postCompleted : (succeeded: Bool, msg: String, data: NSDictionary?) -> ()) {
    var request = NSMutableURLRequest(URL: NSURL(string: url)!)
    var session = NSURLSession.sharedSession()

    request.HTTPMethod = "POST"

    var err: NSError?
    request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)

    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
        printMessage("Response: \(response)", verbosity: .high)
        var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
        printMessage("Body: \(strData)", verbosity: .high)
        var err: NSError?
        var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
        
        var msg = "No message"

        if let parseJSON = json {
            printMessage("JSON Success")
            postCompleted(succeeded: true, msg: "Success", data: parseJSON)
        }

        // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
//        if(err != nil) {
//            printMessage(err!.localizedDescription)
//            let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
//            printMessage("Error could not parse JSON", verbosity: .error)
//            printMessage("JSON: '\(jsonStr)'", verbosity: .debug)
//            postCompleted(succeeded: false, msg: "Error", data: nil)
//        }
//        else {
//            // The JSONObjectWithData constructor didn't return an error. But, we should still
//            // check and make sure that json has a value using optional binding.
//            if let parseJSON = json {
//                // Okay, the parsedJSON is here, let's get the value for 'success' out of it
//                if let success = parseJSON["success"] as? Bool {
//                    printMessage("Succes: \(success)")
//                    postCompleted(succeeded: success, msg: "Success", data: parseJSON)
//                }
//                return
//            }
//            else {
//                // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
//                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
//                printMessage("Error could not parse JSON: \(jsonStr)")
//                postCompleted(succeeded: false, msg: "Error", data : nil)
//            }
//        }
    })
    
    task.resume()
}



func listSubviewsOfView(view:UIView){

    // Get the subviews of the view
    var subviews = view.subviews

    // Return if there are no subviews
    if subviews.count == 0 {
        return
    }

    for subview : AnyObject in subviews{

        // Do what you want to do with the subview
        println(subview)

        // List the subviews of subview
        listSubviewsOfView(subview as! UIView)
    }
}