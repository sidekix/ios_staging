//
//  SidekixMenu.swift
//  SidekixFE
//
//  Created by Sea Pong on 7/30/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

import UIKit

@objc public protocol SidekixMenuDelegate {
    optional func MenuWillOpen()
    optional func MenuWillClose()
    optional func MenuShouldOpenMenu () -> Bool
}

@objc public protocol SidekixMenuProtocol {
    var menu : SidekixMenu? { get }
    func setContentViewController(contentViewController: UIViewController)
}

public enum SidekixMenuAnimation : Int {
    case None
    case Default
}

public enum SidekixMenuPosition : Int {
    case Left
    case Right
}


// Extend UIViewController in this project such that any VC can control the menu
public extension UIViewController {

    /**
    Changes current state of side menu view.
    */
    public func toggleSideMenuView () {

        printMessage("Toggle Side Menu")
        sideMenuController()?.menu?.toggleMenu()
    }
    /**
    Hides the side menu view.
    */
    public func hideSideMenuView () {
        sideMenuController()?.menu?.hideSideMenu()
    }
    /**
    Shows the side menu view.
    */
    public func showSideMenuView () {

        sideMenuController()?.menu?.showSideMenu()
    }

    /**
    Returns a Boolean value indicating whether the side menu is showed.

    :returns: BOOL value
    */
    public func isSideMenuOpen () -> Bool {
        let sideMenuOpen = self.sideMenuController()?.menu?.isMenuOpen
        return sideMenuOpen!
    }

    /**
    * You must call this method from viewDidLayoutSubviews in your content view controlers so it fixes size and position of the side menu when the screen
    * rotates.
    * A convenient way to do it might be creating a subclass of UIViewController that does precisely that and then subclassing your view controllers from it.
    */
    func fixSideMenuSize() {
        if let navController = self.navigationController as? SidekixMenuNavigationController {
            navController.menu?.updateFrame()
        }
    }



    func findViewControllerWithMenu() -> SidekixMenuProtocol?{

        var currentVC = UIApplication.sharedApplication().keyWindow?.rootViewController
        printMessage("Traversing View Controllers - Starting VC = \(currentVC!.title)", verbosity: .debug)


        while let childVC : UIViewController? = currentVC!.presentedViewController {
            printMessage("Traversing View Controllers - currentVC = \(currentVC!.title)", verbosity: .debug)
            printMessage("Traversing View Controllers - childVC   = \(childVC?.title)", verbosity: .debug)

            if currentVC is SidekixMenuProtocol {

                printMessage("Finished Traversing View Controllers - finalVC = \(currentVC!.title)", verbosity: .debug)

                return (currentVC as! SidekixMenuProtocol)
            }
            currentVC = childVC

        }
        return (currentVC as! SidekixMenuProtocol)

    }



    

    /**
    Returns a view controller containing a side menu

    :returns: A `UIViewController`responding to `ENSideMenuProtocol` protocol
    */
    public func sideMenuController () -> SidekixMenuProtocol? {

        printMessage("Finding Side Menu View Controller", verbosity: .debug)

        // start with the parent view controller
        var iteration : UIViewController? = self.parentViewController

        printMessage("Finding Side Menu View Controller - Starting at \(iteration?.title)", verbosity: .debug)

        if (iteration == nil) {
            printMessage("Finding Side Menu View Controller - Can't Find via parent, starting from top most controller", verbosity: .debug)

            if let vc = findViewControllerWithMenu() as SidekixMenuProtocol? {
                printMessage("Finding Side Menu View Controller - Found Starting Point", verbosity: .debug)
                iteration = (vc as! UIViewController)
            }
            else {
                //crash
            }


            //return topMostController()

        }
        do {
            
            if (iteration is SidekixMenuProtocol) {
                printMessage("Finding Side Menu View Controller - Found", verbosity: .debug)
                return iteration as? SidekixMenuProtocol
            } else if (iteration?.parentViewController != nil && iteration?.parentViewController != iteration) {
                printMessage("Finding Side Menu View Controller - Going to Parent \(iteration?.parentViewController?.title)", verbosity: .debug)
                iteration = iteration!.parentViewController
            } else {
                printMessage("Finding Side Menu View Controller - Couldn't Find \(iteration?.title)", verbosity: .debug)
                iteration = nil
            }
        } while (iteration != nil)

        return iteration as? SidekixMenuProtocol
    }

    internal func topMostController () -> SidekixMenuProtocol? {
        var topController : UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController
        if (topController is UITabBarController) {
            topController = (topController as! UITabBarController).selectedViewController
        }
        while (topController?.presentedViewController is SidekixMenuProtocol) {
            topController = topController?.presentedViewController
        }

        return topController as? SidekixMenuProtocol
    }
}

public class SidekixMenu: NSObject, UIGestureRecognizerDelegate {
    /// The width of the side menu view. The default value is 160.
    public var menuWidth : CGFloat = 100.0 {
        didSet {
            needUpdateApperance = true
            updateFrame()
        }
    }
    private var menuPosition:SidekixMenuPosition = .Right
    ///  A Boolean value indicating whether the bouncing effect is enabled. The default value is TRUE.
    public var bouncingEnabled :Bool = false
    /// The duration of the slide animation. Used only when `bouncingEnabled` is FALSE.
    public var animationDuration = 0.4
    private let sideMenuContainerView =  UIView()
    private var menuViewController : UIViewController!
    private var animator : UIDynamicAnimator!
    private var sourceView : UIView!
    private var needUpdateApperance : Bool = false
    /// The delegate of the side menu
    public weak var delegate : SidekixMenuDelegate?
    private(set) var isMenuOpen : Bool = false
    /// A Boolean value indicating whether the left swipe is enabled.
    public var allowLeftSwipe : Bool = true
    /// A Boolean value indicating whether the right swipe is enabled.
    public var allowRightSwipe : Bool = true

    /**
    Initializes an instance of a `ENSideMenu` object.

    :param: sourceView   The parent view of the side menu view.
    :param: menuPosition The position of the side menu view.

    :returns: An initialized `ENSideMenu` object, added to the specified view.
    */
    public init(sourceView: UIView, menuPosition: SidekixMenuPosition) {
        super.init()
        self.sourceView = sourceView
        self.menuPosition = menuPosition
        self.setupMenuView()

        animator = UIDynamicAnimator(referenceView:sourceView)

        // Add right swipe gesture recognizer
        let rightSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleGesture:")
        rightSwipeGestureRecognizer.delegate = self
        rightSwipeGestureRecognizer.direction =  UISwipeGestureRecognizerDirection.Right

        // Add left swipe gesture recognizer
        let leftSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleGesture:")
        leftSwipeGestureRecognizer.delegate = self
        leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirection.Left
/*
        if (menuPosition == .Left) {
            sourceView.addGestureRecognizer(rightSwipeGestureRecognizer)
            sideMenuContainerView.addGestureRecognizer(leftSwipeGestureRecognizer)
        }
        else {
            sideMenuContainerView.addGestureRecognizer(rightSwipeGestureRecognizer)
            sourceView.addGestureRecognizer(leftSwipeGestureRecognizer)
        }
*/

    }
    /**
    Initializes an instance of a `ENSideMenu` object.

    :param: sourceView         The parent view of the side menu view.
    :param: menuViewController A menu view controller object which will be placed in the side menu view.
    :param: menuPosition       The position of the side menu view.

    :returns: An initialized `ENSideMenu` object, added to the specified view, containing the specified menu view controller.
    */
    public convenience init(sourceView: UIView, menuViewController: UIViewController, menuPosition: SidekixMenuPosition) {
        self.init(sourceView: sourceView, menuPosition: menuPosition)
        self.menuViewController = menuViewController
        self.menuViewController.view.frame = sideMenuContainerView.bounds
        self.menuViewController.view.autoresizingMask = .FlexibleHeight | .FlexibleWidth
        sideMenuContainerView.addSubview(self.menuViewController.view)
    }
    /*
    public convenience init(sourceView: UIView, view: UIView, menuPosition: ENSideMenuPosition) {
    self.init(sourceView: sourceView, menuPosition: menuPosition)
    view.frame = sideMenuContainerView.bounds
    view.autoresizingMask = .FlexibleHeight | .FlexibleWidth
    sideMenuContainerView.addSubview(view)
    }
    */
    /**
    Updates the frame of the side menu view.
    */
    func updateFrame() {
        var width:CGFloat
        var height:CGFloat
        (width, height) = adjustFrameDimensions( sourceView.frame.size.width, height: sourceView.frame.size.height)
        let menuFrame = CGRectMake(
            (menuPosition == .Left) ?
                isMenuOpen ? 0 : -menuWidth-1.0 :
                isMenuOpen ? width - menuWidth : width+1.0,
            sourceView.frame.origin.y,
            menuWidth,
            height
        )
        sideMenuContainerView.frame = menuFrame
    }

    private func adjustFrameDimensions( width: CGFloat, height: CGFloat ) -> (CGFloat,CGFloat) {
        if floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1 &&
            (UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.LandscapeRight ||
                UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.LandscapeLeft) {
                    // iOS 7.1 or lower and landscape mode -> interchange width and height
                    return (height, width)
        }
        else {
            return (width, height)
        }

    }

    private func setupMenuView() {

        printMessage("Setting Up Menu View")

        // Configure side menu container
        updateFrame()

        sideMenuContainerView.backgroundColor = UIColor.clearColor()
        sideMenuContainerView.clipsToBounds = false
        sideMenuContainerView.layer.masksToBounds = false
        sideMenuContainerView.layer.shadowOffset = (menuPosition == .Left) ? CGSizeMake(1.0, 1.0) : CGSizeMake(-1.0, -1.0)
        sideMenuContainerView.layer.shadowRadius = 1.0
        sideMenuContainerView.layer.shadowOpacity = 0.125
        sideMenuContainerView.layer.shadowPath = UIBezierPath(rect: sideMenuContainerView.bounds).CGPath

        sourceView.addSubview(sideMenuContainerView)

        if (NSClassFromString("UIVisualEffectView") != nil) {
            // Add blur view
            var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark)) as UIVisualEffectView
            visualEffectView.frame = sideMenuContainerView.bounds
            visualEffectView.autoresizingMask = .FlexibleHeight | .FlexibleWidth
            sideMenuContainerView.addSubview(visualEffectView)
        }
        else {
            // TODO: add blur for ios 7
        }
    }

    private func toggleMenu (shouldOpen: Bool) {
        if (shouldOpen && delegate?.MenuShouldOpenMenu!() == false) {
            return
        }
        updateSideMenuApperanceIfNeeded()
        isMenuOpen = shouldOpen
        var width:CGFloat
        var height:CGFloat
        (width, height) = adjustFrameDimensions( sourceView.frame.size.width, height: sourceView.frame.size.height)
        if (bouncingEnabled) {

            animator.removeAllBehaviors()

            var gravityDirectionX: CGFloat
            var pushMagnitude: CGFloat
            var boundaryPointX: CGFloat
            var boundaryPointY: CGFloat

            if (menuPosition == .Left) {
                // Left side menu
                gravityDirectionX = (shouldOpen) ? 1 : -1
                pushMagnitude = (shouldOpen) ? 20 : -20
                boundaryPointX = (shouldOpen) ? menuWidth : -menuWidth-2
                boundaryPointY = 20
            }
            else {
                // Right side menu
                gravityDirectionX = (shouldOpen) ? -1 : 1
                pushMagnitude = (shouldOpen) ? -20 : 20
                boundaryPointX = (shouldOpen) ? width-menuWidth : width+menuWidth+2
                boundaryPointY =  -20
            }

            let gravityBehavior = UIGravityBehavior(items: [sideMenuContainerView])
            gravityBehavior.gravityDirection = CGVectorMake(gravityDirectionX,  0)
            animator.addBehavior(gravityBehavior)

            let collisionBehavior = UICollisionBehavior(items: [sideMenuContainerView])
            collisionBehavior.addBoundaryWithIdentifier("menuBoundary", fromPoint: CGPointMake(boundaryPointX, boundaryPointY),
                toPoint: CGPointMake(boundaryPointX, height))
            animator.addBehavior(collisionBehavior)

            let pushBehavior = UIPushBehavior(items: [sideMenuContainerView], mode: UIPushBehaviorMode.Instantaneous)
            pushBehavior.magnitude = pushMagnitude
            animator.addBehavior(pushBehavior)

            let menuViewBehavior = UIDynamicItemBehavior(items: [sideMenuContainerView])
            menuViewBehavior.elasticity = 0.0
            animator.addBehavior(menuViewBehavior)

        }
        else {
            var destFrame :CGRect
            if (menuPosition == .Left) {
                destFrame = CGRectMake((shouldOpen) ? -2.0 : -menuWidth, 0, menuWidth, height)
            }
            else {
                destFrame = CGRectMake((shouldOpen) ? width-menuWidth : width+2.0,
                    0,
                    menuWidth,
                    height)
            }

            UIView.animateWithDuration(animationDuration, animations: { () -> Void in
                self.sideMenuContainerView.frame = destFrame
            })
        }

        if (shouldOpen) {
            delegate?.MenuWillOpen!()
        } else {
            delegate?.MenuWillClose!()
        }
    }

    public func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer is UISwipeGestureRecognizer {
            let swipeGestureRecognizer = gestureRecognizer as! UISwipeGestureRecognizer
            if !self.allowLeftSwipe {
                if swipeGestureRecognizer.direction == .Left {
                    return false
                }
            }

            if !self.allowRightSwipe {
                if swipeGestureRecognizer.direction == .Right {
                    return false
                }
            }
        }
        return true
    }

    internal func handleGesture(gesture: UISwipeGestureRecognizer) {
        toggleMenu((self.menuPosition == .Right && gesture.direction == .Left)
            || (self.menuPosition == .Left && gesture.direction == .Right))
    }

    private func updateSideMenuApperanceIfNeeded () {
        if (needUpdateApperance) {
            var frame = sideMenuContainerView.frame
            frame.size.width = menuWidth
            sideMenuContainerView.frame = frame
            sideMenuContainerView.layer.shadowPath = UIBezierPath(rect: sideMenuContainerView.bounds).CGPath

            needUpdateApperance = false
        }
    }

    /**
    Toggles the state of the side menu.
    */
    public func toggleMenu () {
        if (isMenuOpen) {
            printMessage("Close Menu")
            toggleMenu(false)
        }
        else {
            printMessage("Open Menu")
            updateSideMenuApperanceIfNeeded()
            toggleMenu(true)
        }
    }
    /**
    Shows the side menu if the menu is hidden.
    */
    public func showSideMenu () {
        if (!isMenuOpen) {
            toggleMenu(true)
        }
    }
    /**
    Hides the side menu if the menu is showed.
    */
    public func hideSideMenu () {
        if (isMenuOpen) {
            toggleMenu(false)
        }
    }
}
