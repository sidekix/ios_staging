//
//  SidekixMenuNavigationController.swift
//  SidekixFE
//
//  Created by Sea Pong on 1/28/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//
// All Navigation Controllers that wish to access the main Sidekix Menu shall
// extend from this class
//

import UIKit


class SidekixMenuNavigationController: UINavigationController, SidekixMenuProtocol {

    
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var homeExploreButton: UIButton!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!


    @IBOutlet var menuView: UIView!


    var menu : SidekixMenu?

    var sideMenuAnimationType : SidekixMenuAnimation = .Default

    // Default Init
    init( menuViewController: UIViewController, contentViewController: UIViewController?) {
        super.init(nibName: nil, bundle: nil)

        if (contentViewController != nil) {
            self.viewControllers = [contentViewController!]
        }

        menu = SidekixMenu(sourceView: self.view, menuViewController: menuViewController, menuPosition:.Left)
        view.bringSubviewToFront(navigationBar)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }



    // MARK: - Navigation
    func setContentViewController(contentViewController: UIViewController) {
        printMessage("Setting Content View Controller - to \(contentViewController.title)", verbosity: .debug)

        
        self.menu?.hideSideMenu()
        switch sideMenuAnimationType {
        case .None:
            self.viewControllers = [contentViewController]
            break
        default:
            contentViewController.navigationItem.hidesBackButton = true
            
            self.setViewControllers([contentViewController], animated: true)

            break
        }

    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        homeExploreButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
//        addPhotoButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
//        chatButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
//        profileButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit

    }

    
    
    
    @IBAction func exitButtonTouch(sender: AnyObject, forEvent event: UIEvent) {
        
        self.dismissViewControllerAnimated(true, completion: nil)

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
