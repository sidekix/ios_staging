//
//  SidekixUserProfile.swift
//  SidekixFE
//
//  Created by Sea Pong on 9/8/15.
//  Copyright (c) 2015 Sidekix. All rights reserved.
//

class SidekixUserProfile{

    var username : String = ""
    var firstName : String = ""
    var lastName  : String = ""
    var bio : String = ""
    var html : String = ""
    var fbID : String = ""
    var skID : String = ""
    var admin : Bool = false


}